package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.Practica6poo.Lavado;
import ito.poo.clases.Practica6poo.Llenado;
import ito.poo.clases.Practica6poo.Empaquetado;

public class MyApp {
	
	static void run() {
		
		Lavado lav= new Lavado("Maquina de Lavado", LocalDate.of(2021, 7, 25), 150f, 12f, 20);
		Llenado llen= new Llenado("Maquina de LLenado", LocalDate.of(2021, 8, 10), 5000f, 15, 100);
		Empaquetado empaq= new Empaquetado("Maquina de Empaquetado", LocalDate.of(2021, 9, 01), 361f, 9, 3);
		
		System.out.println(lav);
		System.out.println("Costo de lavado por botella: " + lav.costoLavado() + " $ ");
		System.out.println(llen);
		System.out.println("Costo de llenado por botella:" + llen.costoLlenado() + " $ ");
		System.out.println(empaq);
		System.out.println("Costo de empaquetado por botella:" + empaq.costoEmpaquetado() + " $ ");
	
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();

	}

}
